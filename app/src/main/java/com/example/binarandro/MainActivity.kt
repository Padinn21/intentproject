package com.example.binarandro

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.binarandro.`class`.Person
import com.example.binarandro.`class`.User
import com.example.binarandro.databinding.ActivityIntent4Binding
import com.example.binarandro.databinding.ActivityMainBinding
import com.example.binarandro.intent.IntentActivity
import com.example.binarandro.intent.IntentActivity2
import com.example.binarandro.intent.IntentActivity3


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnByone.setOnClickListener {
            val intent = Intent(this, IntentActivity::class.java)
            intent.putExtra("FULL_NAME", "Alif Izzuddin Ramadhan")
            intent.putExtra("NICK_NAME", "Alif")
            intent.putExtra("AGE", 20)
            intent.putExtra("ADDRESS", "Jl.Perjuangan No.20")
            startActivity(intent)

        }

        binding.btnBundle.setOnClickListener{
            val intent = Intent(this, IntentActivity2::class.java)
            val bundle = Bundle()

            bundle.putString("FULL_NAME", "Alif Izzuddin Ramadhan")
            bundle.putString("NICK_NAME", "Alif")
            bundle.putInt("AGE", 20)
            bundle.putString("ADDRESS", "Jl.Perjuangan No.20")
            intent.putExtras(bundle)
            startActivity(intent)
        }


        binding.btnSerializable.setOnClickListener{
            val intent = Intent(this, IntentActivity3::class.java)
            val person = Person("Alif Izzuddin Ramadhan", "Alif", 20, "Jl.Perjuangan No.20")
            intent.putExtra("KEY_PERSON", person)
            startActivity(intent)
        }

        binding.btnParcelable.setOnClickListener{
            val intent = Intent(this, ActivityIntent4Binding::class.java)
            val user = User("Alif Izzuddin Ramadhan", "Alif", 20, "Jl.Perjuangan No.20")
            intent.putExtra("KEY_USER", user)
        }


    }

}