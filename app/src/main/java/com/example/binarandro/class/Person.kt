package com.example.binarandro.`class`

import java.io.Serializable

class Person(fName: String,
             nkName: String,
             age: Int,
             address: String) : Serializable