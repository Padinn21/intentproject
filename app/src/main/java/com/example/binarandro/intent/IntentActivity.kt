package com.example.binarandro.intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.binarandro.databinding.ActivityIntentBinding

class IntentActivity : AppCompatActivity() {

    private lateinit var binding: ActivityIntentBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIntentBinding.inflate(layoutInflater)
        setContentView(binding.root)


        //Get Data
        val fName = intent.getStringExtra("FUll_NAME")
        binding.txtFullName.text = "Hi, My Name is $fName"


        val nkName = intent.getStringExtra("NICK_NAME")
        binding.txtNickName.text = "You can call me $nkName"

        val age = intent.getIntExtra("AGE", 0)
        binding.txtAge.text = "I'm $age years old "

        val address = intent.getStringExtra("ADDRESS")
        binding.txtAddress.text = "And I live at $address, Bekasi"




    }


}