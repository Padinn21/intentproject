package com.example.binarandro.intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.binarandro.databinding.ActivityIntent2Binding

class IntentActivity2 : AppCompatActivity() {

    private lateinit var binding : ActivityIntent2Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIntent2Binding.inflate(layoutInflater)
        setContentView(binding.root)

        val bundle = intent.extras
        val fName = bundle?.getString("FULL_NAME")
        val nkName = bundle?.getString("NICK_NAME")
        val age = bundle?.getInt("AGE")
        val address = bundle?.getString("ADDRESS")

        binding.txtFullName.text = "Hi, My Name is $fName"
        binding.txtNickName.text = "You can call me $nkName"
        binding.txtAge.text = "I'm $age years old "
        binding.txtAddress.text = "And I live at $address, Bekasi"


    }
}