package com.example.binarandro.intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.binarandro.`class`.Person
import com.example.binarandro.databinding.ActivityIntent3Binding

class IntentActivity3 : AppCompatActivity() {

    private lateinit var binding: ActivityIntent3Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIntent3Binding.inflate(layoutInflater)
        setContentView(binding.root)

        val person = intent.getSerializableExtra("KEY_PERSON") as Person



    }
}