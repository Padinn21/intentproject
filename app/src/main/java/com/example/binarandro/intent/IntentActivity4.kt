package com.example.binarandro.intent

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.binarandro.`class`.User
import com.example.binarandro.databinding.ActivityIntent4Binding

class IntentActivity4 : AppCompatActivity() {

    private lateinit var binding: ActivityIntent4Binding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityIntent4Binding.inflate(layoutInflater)
        setContentView(binding.root)

        val user = intent.getParcelableExtra<User>("KEY_USER")
    }
}